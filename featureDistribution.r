#!/usr/bin/env Rscript

library(DBI)
library(RMySQL)

con <- dbConnect(MySQL(),
	   user = "root", password = "root",
       dbname = "baseballStats", host = "localhost")

"Distribution of the 4 selected features"
rs <- dbSendQuery(con, "SELECT W, WHIP, ERA, SO FROM TrainingSet_2005")
data <- fetch(rs, n=-1)
dbDisconnect(con)

"Normalization"
# data$W <- scale(data$W)
# data$ERA <- scale(data$ERA)
# data$WHIP <- scale(data$WHIP)
# data$SO <- scale(data$SO)

"Plot Histogram"
par(mfrow = c(2, 2))
hist(data$W, xlab = "W", main = "W Stat in Year 2005", prob = TRUE)
lines(density(data$W, bw = 1)) #bw = "SJ" for band width auto-selection
rug(data$W)
hist(data$WHIP, xlab = "WHIP", main = "WHIP Stat in Year 2005")
rug(data$WHIP)
hist(data$ERA, xlab = "ERA", main = "ERA Stat in Year 2005")
rug(data$ERA)
hist(data$SO, xlab = "SO", main = "SO Stat in Year 2005", prob = TRUE)
lines(density(data$SO, bw = "SJ")) #bw = "SJ" for band width auto-selection
rug(data$SO)

"Normality Test"

"QQ Plot"
par(mfrow = c(2, 2))
qqnorm(data$W, main = "Normal Q-Q Plot on W")
qqline(data$W)
qqnorm(data$WHIP, main = "Normal Q-Q Plot on WHIP")
qqline(data$WHIP)
qqnorm(data$ERA, main = "Normal Q-Q Plot on ERA")
qqline(data$ERA)
qqnorm(data$SO, main = "Normal Q-Q Plot on SO")
qqline(data$SO)

"Sampling"
sample_W <- sample(data$W, 30)
sample_WHIP <- sample(data$WHIP, 30)
"-- Mirroring"
for (i in sample_W)
	sample_W <<- c(sample_W, -i)

"Shapiro-Wilk Test on W"
test_SW <- shapiro.test(sample_W)
print(test_SW)

"Student's Test on W"
test_S <- t.test(sample_W)
print(test_S)

"Tests on other features, WHIP, ERA and SO, can be performed in similar ways."

"Chi-squared Test on WHIP"
test_chisq <- chisq.test(sample_WHIP)
print(test_chisq)