# DM-Baseball

This is a **Data Mining** project on **baseball** under supervision of [Irwin King](https://www.cse.cuhk.edu.hk/irwin.king/), CSE, Faculty of Engineering, CUHK.

## To-do

* Implement a function to calculate the **Gini Coefficient** of a decision tree, since there's no original module in *cpart* to evaluate the quality of such tree.
* Find out the underneath relevance between the features, and try to combine them if relevant. Thanks to @liyi3c for bringing this up.


## Jobs Already Done

Most recent on top.

* A R-script is written to generate a decision tree for the sake of classifying WBC pitchers
* A distribution research for selected 4 features(W, WHIP, ERA, SO)
* [ABANDONED]A lame Bayesian classifier which predicts the WBC starting pitchers for team U.S.A
* Merging and joining on table *Pitch*, *Master*, *Roster*, and *Salary* to build the training set for classifier
* A python crawler which can fetch previous WBC roasters from [Baseball Reference ](http://www.baseball-reference.com/bullpen/2009_World_Baseball_Classic_(Rosters))

## Known Issues

* Remove Salary as a feature. It is a reflection of the ability of a player, rather than a determinant factor. 

## Primarily Used Languages

Python, R.